# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils linux-info udev versionator

MY_PV="$(get_version_component_range 1-3)AR.$(get_version_component_range 4)"
MY_P="${PN/3g-ar/-3g_ntfsprogs}-${MY_PV}"

DESCRIPTION="NTFS-3G Advanced variant supporting ACLs, junction points, compression and more"
HOMEPAGE="http://jp-andre.pagesperso-orange.fr/advanced-ntfs-3g.html"
SRC_URI="http://jp-andre.pagesperso-orange.fr/${MY_P}.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="acl crypt debug +external-fuse +ntfsprogs static-libs suid xattr"

RDEPEND="!<sys-apps/util-linux-2.20.1-r2
	!sys-fs/ntfsprogs
	crypt? (
		>=dev-libs/libgcrypt-1.2.2
		>=net-libs/gnutls-1.4.4
		)
	external-fuse? ( >=sys-fs/fuse-2.8.0 )"
DEPEND="${RDEPEND}
	sys-apps/attr
	virtual/pkgconfig"

S=${WORKDIR}/${MY_P}

DOCS="AUTHORS ChangeLog CREDITS README"

pkg_setup() {
	if use external-fuse && use kernel_linux; then
		if kernel_is lt 2 6 9; then
			die "Your kernel is too old."
		fi
		CONFIG_CHECK="~FUSE_FS"
		FUSE_FS_WARNING="You need to have FUSE module built to use ntfs-3g"
		linux-info_pkg_setup
	fi
}

src_prepare() {
	# add missing $(sbindir) references
	sed -e 's:sbin\($\|/\):$(sbindir)\1:g' \
		-i ntfsprogs/Makefile.in src/Makefile.in || die
}

src_configure() {
	econf \
		--prefix="${EPREFIX}"/usr \
		--exec-prefix="${EPREFIX}"/usr \
		--docdir="${EPREFIX}"/usr/share/doc/${PF} \
		$(use_enable debug) \
		--enable-ldscript \
		--disable-ldconfig \
		$(use_enable acl posix-acls) \
		$(use_enable xattr xattr-mappings) \
		$(use_enable crypt crypto) \
		$(use_enable ntfsprogs) \
		--without-uuid \
		--enable-extras \
		$(use_enable static-libs static) \
		--with-fuse=$(usex external-fuse external internal)
}

src_install() {
	default

	use suid && fperms u+s /usr/bin/${MY_PN}
	udev_dorules "${FILESDIR}"/99-ntfs3g.rules
	prune_libtool_files

	# http://bugs.gentoo.org/398069
	rmdir "${D}"/sbin

	dosym mount.ntfs-3g /usr/sbin/mount.ntfs #374197
}

pkg_pretend() {
	if [[ ${MERGE_TYPE} != binary ]]; then
		# Bug 450024
		if $(tc-getLD) --version | grep -q "GNU gold"; then
			eerror "ntfs-3g does not function correctly when built with the gold linker."
			eerror "Please select the bfd linker with binutils-config."
			die "GNU gold detected"
		fi
	fi
}

pkg_postinst() {
	ewarn "This is an advanced features release of the ntfs-3g package. It"
	ewarn "passes standard tests on i386 and x86_64 CPUs but users should"
	ewarn "still backup their data.  More info at:"
	ewarn "http://jp-andre.pagesperso-orange.fr/advanced-ntfs-3g.html"

	if use suid; then
		ewarn
		ewarn "You have chosen to install ${PN} with the binary setuid root. This"
		ewarn "means that if there any undetected vulnerabilities in the binary,"
		ewarn "then local users may be able to gain root access on your machine."
	fi
}
