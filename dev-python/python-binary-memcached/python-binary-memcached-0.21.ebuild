# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Pure Python module (thread safe) to use memcached via it's binary protocol with SASL auth support"
HOMEPAGE="https://github.com/jaysonsantos/python-binary-memcached"
SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test doc"

DEPEND="
	test? ( net-misc/memcached[sasl] dev-libs/cyrus-sasl dev-python/pytest dev-python/mock )
	doc? ( dev-python/sphinx )"

src_compile() {
	distutils-r1_src_compile
	if use doc; then
		emake -C docs html
	fi
}

src_install() {
	distutils-r1_src_install

	if use doc; then
		einfo 'installing documentation'
		dohtml -r docs/_build/html/*
	fi
}

python_test() {
	memcached &> /dev/null &
	local general_memcached_pid=$!

	memcached -p5000 &> /dev/null &
	local non_standard_port_memcached_pid=$!

	memcached -s /tmp/memcached.sock&
	local sock_memcached_pid=$!

	py.test
	local ret=$?

	kill $general_memcached_pid
	kill $non_standard_port_memcached_pid
	kill $sock_memcached_pid
	test 0 -eq $ret || die "Stopping because tests failed"
}
