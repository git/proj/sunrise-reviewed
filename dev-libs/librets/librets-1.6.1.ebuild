# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

PHP_EXT_OPTIONAL_USE="php"
PHP_EXT_NAME="librets"
PHP_EXT_SKIP_PHPIZE="yes"
USE_PHP="php5-4 php5-5"

PYTHON_COMPAT=( python2_7 python3_{2,3} )

USE_RUBY="ruby19 ruby20"
RUBY_OPTIONAL="yes"

inherit autotools eutils java-pkg-opt-2 mono-env perl-module php-ext-source-r2 python-r1 ruby-ng toolchain-funcs

DESCRIPTION="A library that implements the RETS 1.8, 1.7, 1.5 and 1.0 standards"
HOMEPAGE="http://www.crt.realtors.org/projects/rets/librets/"
SRC_URI="http://www.crt.realtors.org/projects/rets/${PN}/files/${P}.tar.gz"

LICENSE="BSD-NAR"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc java mono perl php python ruby sql-compiler threads"
# Enabling thread safety for perl, php, python or ruby causes segmentation faults in cli scripts but not through apache
REQUIRED_USE="mono? ( sql-compiler )
	perl? ( !threads )
	php? ( !threads )
	python? ( !threads )
	ruby? ( !threads )"

SWIG_RDEPEND="dev-libs/libgcrypt
	dev-libs/libgpg-error
	dev-libs/libtasn1
	net-dns/libidn
	net-libs/gnutls"

RDEPEND=">=dev-libs/boost-1.46
	dev-libs/expat
	>=dev-util/boost-build-1.46
	net-misc/curl
	sql-compiler? ( dev-java/antlr:0[script] )
	sys-libs/zlib
	java? ( >=virtual/jdk-1.6.0 ${SWIG_RDEPEND} )
	mono? ( dev-lang/mono ${SWIG_RDEPEND} )
	php? ( ${SWIG_RDEPEND} )
	python? ( ${SWIG_RDEPEND} ${PYTHON_DEPS} )
	ruby? ( $(ruby_implementations_depend) ${SWIG_RDEPEND} )"

DEPEND="java? ( >=dev-lang/swig-1.3.40-r1 )
	mono? ( >=dev-lang/swig-1.3.40-r1 )
	php? ( dev-lang/php[-threads] >=dev-lang/swig-1.3.40-r1 )
	python? ( >=dev-lang/swig-1.3.40-r1 )
	ruby_targets_ruby19? ( >=dev-lang/swig-2.0.4-r1 )
	ruby_targets_ruby20? ( >=dev-lang/swig-2.0.4-r1 )
	${RDEPEND}"

# Reset to the default $S since ruby-ng overrides it
S="${WORKDIR}/${P}"

unset SWIG_RDEPEND

# Since php-ext-source-r2_src_install tries to install non-existant headers
# and a bad emake fails on EAPI 4, a copied subset must be used instead (bug 404307).
my_php-ext-source-r2_src_install() {
	local slot
	for slot in $(php_get_slots); do
		php_init_slot_env ${slot}
		# Let's put the default module away
		insinto "${EXT_DIR}"
		newins "modules/${PHP_EXT_NAME}.so" "${PHP_EXT_NAME}.so"
	done
	php-ext-source-r2_createinifiles
}

my_php-move_swig_build_to_modules_dir() {
	mkdir "${1}"/modules || die "Could not create directory for php slot"
	mv build/swig/php5/* "${1}"/modules || die "Could not move php slot build"
}

my_php-replace_config_with_selected_config() {
	php_init_slot_env ${1}
	cd "${S}" || die "cannot change to source directory"
	# Replace the reference to php-config with the current slotted one
	sed -i -e "s|${2}|${PHPCONFIG}|g" project/build/php.mk || die "sed php-config change failed"
}

my_ruby-move_swig_build_to_impl_dir() {
	mkdir -p "${1}"/${P} || die "Could not create directory for ruby implementation"
	mv build/swig/ruby/* "${1}"/${P} || die "Could not move ruby implementation build"
}

my_python3-move_swig_build_to_impl_dir() {
	# Only move if something there are files in the python3 directory
	if [ "$(ls -A build/swig/python3)" ]; then
		mkdir -p build/swig/"${1}" || die "Could not create directory for python3 implementation"
		cp -R build/swig/python3/* build/swig/"${1}" || die "Could not move python3 implementation build"
		rm -r build/swig/python3/build || die "Could not remove python3 build"
	fi
}

pkg_setup() {
	use java && java-pkg-opt-2_pkg_setup
	use mono && mono-env_pkg_setup
	use ruby && ruby-ng_pkg_setup
}

src_unpack() {
	use php && php-ext-source-r2_src_unpack
	default
}

src_prepare() {
	epatch "${FILESDIR}"/${P}-fix-php-build-errors.patch \
		"${FILESDIR}"/${P}-extconf.rb.patch \
		"${FILESDIR}"/${P}-java.mk.patch
	epatch_user
	eautoreconf

	if use python; then
		# Change the path to librets-config-inplace for python slotted build support
		sed -i -e "s|../../..|${S}|" project/swig/python/setup.py project/swig/python3/setup.py || die
		# Remove the build-lib path for python slotted build support
		sed -i -e "s| --build-lib=.||" project/build/python3.mk || die
	fi

	use php && php-ext-source-r2_src_prepare
}

src_configure() {
	local myphpprefix
	if use php; then
		# Enable php extension when it finds the current selected slot
		myphpprefix="${PHPPREFIX}/include"
	fi

	# The build system just finds "python", which could be any version of python if EPYTHON is unset.
	use python && python_export_best EPYTHON

	if use ruby; then
		MYRUBYIMPLS=($(ruby_get_use_implementations))
		MYRUBYFIRSTIMPL=${MYRUBYIMPLS[0]}
		# Set RUBY value in config to the first ruby implementation to build
		RUBY=$(ruby_implementation_command ${MYRUBYFIRSTIMPL})
		MYRUBYIMPLS=(${MYRUBYIMPLS[@]:1})
	fi

	# Allow cross-compiling between operating systems since ar is not portable
	tc-export AR
	econf \
		--enable-shared_dependencies \
		--enable-depends \
		--enable-default-search-path="/usr /opt ${myphpprefix}" \
		--disable-examples \
		--disable-debug \
		$(usex doc "--enable-maintainer-documentation") \
		$(usex java "" "--disable-java") \
		$(usex mono "" "--disable-dotnet") \
		$(usex perl "" "--disable-perl") \
		$(usex php "" "--disable-php") \
		$(usex python "" "--disable-python") \
		$(usex threads "--enable-thread-safe") \
		$(use_enable sql-compiler) \
		$(use_with mono "snk-file" "${FILESDIR}"/${PN}.snk) \
		$(usex ruby " RUBY=${RUBY}" "--disable-ruby")
}

src_compile() {
	if use php; then
		local slot myphpconfig="php-config"
		# Shift off the first slot so it doesn't get built again
		local myphpslots=($(php_get_slots)) myphpfirstslot="${myphpslots[@]:0:1}" myphpslots=(${myphpslots[@]:1})
		my_php-replace_config_with_selected_config ${myphpfirstslot} ${myphpconfig}
		myphpconfig="${PHPCONFIG}"
	fi
	emake
	if use php; then
		# Move the current slotted build of php to another dir so other slots can be built
		my_php-move_swig_build_to_modules_dir "${WORKDIR}/${myphpfirstslot}"
		# Build the remaining slots
		for slot in ${myphpslots[@]}; do
			my_php-replace_config_with_selected_config ${slot} ${myphpconfig}
			myphpconfig="${PHPCONFIG}"
			# Build the current slot
			emake build/swig/php5/${PN}.so
			my_php-move_swig_build_to_modules_dir "${PHP_EXT_S}"
		done
	fi

	if use python; then
		local selectedPython3=$(eselect python show --python3)
		my_python3-move_swig_build_to_impl_dir ${selectedPython3}
		building() {
			# Skip python 2.7 and the selected python3 interpreter since it was already built when make ran the first time
			if [ "${EPYTHON:6:1}" = "2" ] || [ "${EPYTHON}" = "${selectedPython3}" ]; then
				return 0
			fi

			pushd "${S}"/build/swig/python3 || die
			"${PYTHON}" setup.py build || die
			popd
			my_python3-move_swig_build_to_impl_dir ${EPYTHON}
		}
		python_foreach_impl building
	fi

	if use ruby; then
		# Move the current implementation build of ruby to another dir so other implementations can be built
		my_ruby-move_swig_build_to_impl_dir "${WORKDIR}/${MYRUBYFIRSTIMPL}"
		unset MYFIRSTRUBYIMPL
		unset RUBY
		local impl
		MYRUBYIMPL="\${RUBY}"
		# Build the remaining implementations
		for impl in ${MYRUBYIMPLS[@]}; do
			cd "${S}" || die "cannot change to source directory"
			# Replace the reference to ${RUBY} with the current implementation
			sed -i -e "s|${MYRUBYIMPL}|$(ruby_implementation_command ${impl})|g" \
				project/build/ruby.mk || die "sed ruby implementation change failed"
			MYRUBYIMPL="$(ruby_implementation_command ${impl})"
			# Build the current implementation
			emake build/swig/ruby/${PN}_native.bundle
			my_ruby-move_swig_build_to_impl_dir "${WORKDIR}/${impl}"
		done
		unset MYRUBYIMPL
		unset MYRUBYIMPLS
		unset impl
	fi
}

each_ruby_install() {
	exeinto "$(ruby_rbconfig_value archdir)"
	doexe "${S}"/librets_native.so
	doruby "${S}"/librets.rb
}

src_install() {
	dolib.a build/${PN}/lib/${PN}.a

	insinto /usr/include
	doins -r project/${PN}/include/${PN}

	dobin "${PN}-config"

	if use php; then
		my_php-ext-source-r2_src_install
		insinto /usr/share/php
		doins "${WORKDIR}"/php"${PHP_CURRENTSLOT}"/modules/${PN}.php
	fi

	if use perl; then
		# Install manually since the package install has sandbox violations
		insinto ${SITE_ARCH}
		insopts "-m755"
		doins -r "${S}"/build/swig/perl/blib/arch/auto
		insopts "-m644"
		doins "${S}"/build/swig/perl/${PN}.pm
	fi

	if use java; then
		java-pkg_dojar "${S}"/build/swig/java/${PN}.jar
		LIBOPTIONS="-m755" java-pkg_doso "${S}"/build/swig/java/${PN}.so
	fi

	use ruby && ruby-ng_src_install

	if use mono; then
		local dll="${S}"/build/swig/csharp/${PN}-dotnet.dll
		gacutil -i "${dll}" \
			-root "${ED}"/usr/$(get_libdir) \
			-gacdir /usr/$(get_libdir) \
			-package ${PN} \
			|| die "installing ${dll} into the Global Assembly Cache failed"
	fi

	if use python; then
		python_install() {
			local pythonversion
			# build files for python 3 reside in folders with slot versions
			if [ "${EPYTHON:6:1}" = "3" ]; then
				pythonversion=${EPYTHON:6}
			fi
			pushd "${S}"/build/swig/python${pythonversion} || die
			"${PYTHON}" setup.py install --root="${D}" || die
			popd
		}
		python_foreach_impl python_install
	fi
}

pkg_preinst() {
	:
}

pkg_postinst() {
	:
}

pkg_prerm() {
	:
}

pkg_postrm() {
	:
}
